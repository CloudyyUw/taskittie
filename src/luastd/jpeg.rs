use oxipng::{InFile, Options, OutFile};
use rlua::{Context, Function};
use std::path::PathBuf;

pub fn jpeg_compress(luactx: Context) -> Function {
    luactx
        .create_function(|_, (input, output): (String, String)| {
            let in_file: InFile = InFile::Path(PathBuf::from(input));
            let out_file: OutFile = OutFile::Path(Some(PathBuf::from(output)));
            let opts: Options = Options::default();
            oxipng::optimize(&in_file, &out_file, &opts).unwrap();
            Ok(())
        })
        .unwrap()
}
