use rlua::{Context, Function, UserData, UserDataMethods};
use std::{
    fs,
    fs::Metadata,
    path::{Path, PathBuf},
    time::SystemTime,
};

struct Res {
    error: bool,
    message: &'static str,
    data: Option<Metadata>,
    filepath: String,
}

impl UserData for Res {
    fn add_methods<'lua, T: UserDataMethods<'lua, Self>>(methods: &mut T) {
        methods.add_method(
            "err",
            |_, this, _: ()| {
                if this.error {
                    Ok(this.message)
                } else {
                    Ok("")
                }
            },
        );
        methods.add_method("is_file", |_, this, _: ()| {
            Ok(this.data.to_owned().unwrap().is_file())
        });
        methods.add_method("is_dir", |_, this, _: ()| {
            Ok(this.data.to_owned().unwrap().is_dir())
        });
        methods.add_method("is_symlink", |_, this, _: ()| {
            Ok(this.data.to_owned().unwrap().is_symlink())
        });
        methods.add_method("is_readonly", |_, this, _: ()| {
            Ok(this.data.to_owned().unwrap().permissions().readonly())
        });
        methods.add_method("modified", |_, this, _: ()| {
            Ok(this
                .data
                .to_owned()
                .unwrap()
                .modified()
                .unwrap()
                .duration_since(SystemTime::UNIX_EPOCH)
                .expect("Time went backwards")
                .as_millis())
        });
        methods.add_method("created", |_, this, _: ()| {
            Ok(this
                .data
                .to_owned()
                .unwrap()
                .created()
                .unwrap()
                .duration_since(SystemTime::UNIX_EPOCH)
                .expect("Time went backwards")
                .as_millis())
        });
        methods.add_method("accessed", |_, this, _: ()| {
            Ok(this
                .data
                .to_owned()
                .unwrap()
                .accessed()
                .unwrap()
                .duration_since(SystemTime::UNIX_EPOCH)
                .expect("Time went backwards")
                .as_millis())
        });
        methods.add_method("filename", |_, this, _: ()| {
            let filename = Path::new(&this.filepath)
                .file_name()
                .unwrap()
                .to_str()
                .unwrap();
            Ok(filename.to_owned())
        })
    }
}

pub fn file_metadata(lua: Context) -> Function {
    lua.create_function(|_, file_path: String| {
        if !Path::new(&file_path).exists() {
            Ok(Res {
                error: true,
                message: "File not found",
                data: None,
                filepath: file_path,
            })
        } else {
            let data = fs::metadata(&file_path).unwrap();
            Ok(Res {
                error: false,
                message: "",
                data: Some(data),
                filepath: file_path,
            })
        }
    })
    .unwrap()
}
