use notify::{
    event::{AccessKind, CreateKind, ModifyKind, RemoveKind},
    Config, EventKind, RecommendedWatcher, RecursiveMode, Watcher,
};
use std::{env, fs, io::Write, path, thread};
use toml::Value;

mod luahandler;
#[cfg(not(no_luastd))]
mod luastd;

fn main() -> std::io::Result<()> {
    // ~/.config/taskittie
    let config_dir = format!(
        "{}/taskittie/",
        dirs::config_dir()
            .unwrap()
            .into_os_string()
            .into_string()
            .unwrap()
    );
    let config_file = format!("{}taskittie", config_dir);
    let plugins_dir = format!("{}plugins/", config_dir);

    if !path::Path::new(&plugins_dir).exists() {
        fs::create_dir_all(&plugins_dir).expect(&format!("[!!] Error creating {}", plugins_dir));
    }

    if !path::Path::new(&config_file).exists() {
        let mut conf_file =
            fs::File::create(&config_file).expect("[!!] Error when writing config file");
        conf_file.write_all(b"[config]\n# Path to listen for changes\nwatch = \"/home/user/example/folder\"\n# Execute plugins in another thread\nnew_thread = true\n# WIP\n[plugins]\ncustom_path = \"\"\nverify_sig = true\nwhitelist_std = []\nevent_ignore = []")
            .expect("[!!] Error when writing config file");

        println!(
            "\nSettings files successfully created. Please edit them in {} before running again",
            config_file
        );
        std::process::exit(0);
    }

    let parsed_config = fs::read_to_string(config_file)
        .unwrap()
        .parse::<Value>()
        .unwrap();
    let args: Vec<_> = env::args().collect();
    let watch_path: &str;

    if args.len() > 1 {
        watch_path = &args[1];
    } else {
        watch_path = parsed_config["config"]["watch"].as_str().unwrap();
    }

    println!("Watching: {}", watch_path);
    if let Err(err) = watch(
        watch_path,
        parsed_config["config"]["new_thread"].as_bool().unwrap(),
        plugins_dir,
    ) {
        println!("[!!] Watch error: {:?}", err)
    }

    Ok(())
}

fn watch<P: AsRef<path::Path>>(
    path: P,
    new_thread: bool,
    plugins_folder: String,
) -> notify::Result<()> {
    let (tx, rx) = std::sync::mpsc::channel();
    let mut watcher = RecommendedWatcher::new(tx, Config::default())?;

    watcher.watch(path.as_ref(), RecursiveMode::Recursive)?;

    for res in rx {
        match res {
            Ok(event) => {
                let event_type = match &event.kind {
                    EventKind::Access(x) => match x {
                        AccessKind::Read => "file.read",
                        AccessKind::Open(_) => "file.open",
                        AccessKind::Close(_) => "file.close",
                        _ => "file.other",
                    },
                    EventKind::Create(x) => match x {
                        CreateKind::File => "create.file",
                        CreateKind::Folder => "create.folder",
                        _ => "create.other",
                    },
                    EventKind::Modify(x) => match x {
                        ModifyKind::Data(_) => "modify.data",
                        ModifyKind::Metadata(_) => "modify.metadata",
                        ModifyKind::Name(_) => "modify.rename",
                        _ => "modify.other",
                    },
                    EventKind::Remove(x) => match x {
                        RemoveKind::File => "remove.file",
                        RemoveKind::Folder => "remove.folder",
                        _ => "remove.other",
                    },
                    _ => "invalid",
                };
                let plugins_path = plugins_folder.to_owned();
                if new_thread {
                    thread::spawn(|| {
                        luahandler::exec(
                            event_type,
                            event
                                .paths
                                .into_iter()
                                .map(|x| x.to_str().unwrap().to_owned())
                                .collect(),
                            plugins_path,
                        )
                        .expect("[!!] Lua execution error");
                    });
                } else {
                    luahandler::exec(
                        event_type,
                        event
                            .paths
                            .into_iter()
                            .map(|x| x.to_str().unwrap().to_owned())
                            .collect(),
                        plugins_path,
                    )
                    .expect("[!!] Lua execution error");
                }
            }
            Err(e) => panic!("Err: {:?}", e),
        }
    }

    Ok(())
}
