use rlua::{Function, Lua};
use std::{fs, io, path::PathBuf};

#[cfg(not(no_luastd))]
use crate::luastd;

fn load_files(plugins_folder: String) -> io::Result<Vec<PathBuf>> {
    let results = fs::read_dir(plugins_folder)?
        .into_iter()
        .filter(|x| x.is_ok())
        .map(|x| x.unwrap().path())
        .filter(|x| x.is_file())
        .collect();
    Ok(results)
}

pub fn exec(event: &str, paths: Vec<String>, plugins_dir: String) -> rlua::Result<()> {
    for file in load_files(plugins_dir).unwrap() {
        let file_content = fs::read_to_string(file).expect("Error reading lua file");

        Lua::new().context(|lua| {
            lua.load(&file_content).exec().expect("[!!] Lua load error");
            let globals = lua.globals();

            globals.set("EVENT_TYPE", event).unwrap();
            globals.set("EVENT_PATHS", paths.clone()).unwrap();

            #[cfg(not(no_luastd))]
            globals
                .set("jpeg_compress", luastd::jpeg::jpeg_compress(lua))
                .unwrap();
            #[cfg(not(no_luastd))]
            globals
                .set("file_metadata", luastd::file_metadata::file_metadata(lua))
                .unwrap();

            let exec: Function = globals.get("Exec").unwrap();

            exec.call::<_, ()>(()).expect("[!!] Lua Runtime Error");
        })
    }

    Ok(())
}
