# Taskittie

_A simple but powerful automation tool written in Rust and Lua_

Taskittie can automate tasks based on system events (only filesystem currently) with simple Lua scripts.

> This program still in development, so expect some bugs or messy code.

## Usage

When running for the first time, Taskittie will check if the config folder, config file and plugins folder exists, otherwise they will be created at `~/.config/taskittie`.

Lua scripts are located at `~/.config/taskittie/plugins` folder. See [examples](./examples/) for more details.

Change `config.toml` file as you want before running Taskittie again.

An example config: 

```toml
[config]
watch = "/home/user/Downloads"
# Execute plugins in another thread
new_thread = true
```

## Build

With [Luastd](./src/luastd)

```sh
make build
```
Without [Luastd](./src/luastd)

```sh 
make build_nostd
```

Then

```sh
make install
```

## WIP

- Support for more system events
- A GUI for managing plugins
- More Documentation

## License

Copyright (C) 2023 CloudyyUw

Taskittie is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

Please see [LICENSE](./LICENSE) for more details.