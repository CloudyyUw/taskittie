BINPATH=${HOME}/.local/bin/
OUTPATH=${PWD}/target/release/taskittie

build:
	cargo build --release

build_nostd:
	cargo build --release --cfg no_luastd

install:
	mv $OUTPATH $BINPATH
