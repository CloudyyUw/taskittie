-- Compress every image created in a folder to jpeg

-- Credit https://gist.github.com/kgriffs/124aae3ac80eefe57199451b823c24ec
function string:endswith(ending)
  return ending == "" or self:sub(- #ending) == ending
end

function Exec()
  -- print(EVENT_TYPE)
  -- print(EVENT_PATHS[1])
  local filepath = EVENT_PATHS[1]
  local metadata = file_metadata(filepath)

  if EVENT_TYPE == "create.file" then
    if string.len(metadata:err()) > 1 then
      print(metadata:err())
    else
      local filename = metadata:filename()
      if filename == "File not found" then
        print("File moved or deleted")
      end

      if filename:endswith(".png") then
        print(string.format("Compressing %s", filename))
        jpeg_compress(filepath, filepath .. ".jpeg")
      end
    end
  end
end
